# Cookiecutter Project Template


_A logical, reasonably standardized, but flexible project structure for doing and sharing data science work._


#### Project template created from [Drivendata project](http://drivendata.github.io/cookiecutter-data-science/) and [Jbusecke project](https://github.com/jbusecke/cookiecutter-science-project)


## Requirements to use the cookiecutter template:

 - Python 2.7 or 3.5+
 - [Cookiecutter Python package](http://cookiecutter.readthedocs.org/en/latest/installation.html) >= 1.4.0: This can be installed with pip by or conda depending on how you manage your Python packages:

  ```bash
  $ pip install cookiecutter
  ```

  or

  ```bash
  $ conda config --add channels conda-forge
  $ conda install cookiecutter
  ```


## Quickstart

### Create a new project
Just run:
``` bash
cookiecutter https://gitlab.com/public-lh/cookiecutter-project-template
```
(*this should be run from the location that you want the project folder to live, or you will need to move the directory around later.*)

If you have previously created a package with this template confirm the prompt to redownload the newest version.
The installation dialog will ask for a few inputs:
- `project_name`: The name of the project. **If you want to associate this project with a gitlab repository, this should be the same name as the repository name on gitlab for consistency (e.g. my_awesome_project).
- `author_name`: Your name / your company name.
- `GitLab_username`: Your username on the gitlab server.
- `description`: A short description of the project for the readme.
- `open_source_license`: Chose a license for your package. Currently available licenses are: "MIT" and "BSD-3-Clause", details can be found [here]().
- `python_interpreter`: Chose your python version. In most cases just press enter to chose the default version, python 3.
> Unfortunately there seems to be a bug that does [not allow backspace](https://github.com/audreyr/cookiecutter/issues/875) in cookiecutter on certain platforms. If you make a typo cancel the input `ctrl+c` and start over again.


### The resulting directory structure


The directory structure of your new project looks like this:

```

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── Makefile           <- Makefile with commands like `make format` or `make check`,
    │                         additional conda package may have to be installed (e.g. flake8)
    ├── environment.yml    <- Conda environment file. Create environment with
    │                         `conda env create -f environment.yml`
    ├── setup.py           <- makes project pip installable (pip install -e .)
    |                         so source code can be imported    
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see README.md and sphinx-doc.org for details
    │   └── README.md      <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── project_name       <- Source code for use in this project.
    │   ├── __init__.py    <- Makes project_name a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then to use
    │   │   │                 trained models to make predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │         
    │   ├── tests          <- Functions to test the python modules
    │   │   └── test_dummy.py
    │   │   
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io

```
------------

### Setting up a GitLab repository

[Create a new project on GitLab](https://gitlab.com/projects/new), using the same `project_name`. Don't choose *initialize repository with a README*.

Then on the next page, follow the instructions to **Create a new repository**. In a terminal, just do:

```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/[username]/[project_name].git
git add .
git commit -m "Initial commit"
git push -u origin master
```

To not have to enter your GitLab username and password each time your push changes on GitLab, you can run this one-time command in a terminal before pushing your changes:
```shell
git config credential.helper store
```


*Note: On Github it is possible to create automatically the remote repository without
according to the reply to the installation dialog box, using [GitHub Command Line Interface](https://github.com/cli/cli#github-cli). Can the same thing be done on GitLab?*


### Configuring the conda environment

Now you can configure the packages you will need by editing the `environment.yml` file and then, create a conda environment using:

```shell
$ conda env create -f environment.yml
```

:warning: *Note: It can take several minutes to solve the environment dependencies*

No you can now go ahead and start your scientific analysis with the following steps.

```shell
cd project_name
conda activate <project_name>
jupyter-lab
```

### More

More information on how to use cookiecutter template for data science project can be found here: https://github.com/jbusecke/cookiecutter-science-project
