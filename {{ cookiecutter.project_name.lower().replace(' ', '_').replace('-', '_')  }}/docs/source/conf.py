# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import pathlib
import sys


sys.path.insert(0, os.path.abspath("../../"))

import {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}  # isort:skip

# -- Project information -----------------------------------------------------

project = "{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}"
copyright = "2022, {{cookiecutter.author_name}}"
author = "{{cookiecutter.author_name}}"

# # The version info for the project you're documenting, acts as replacement for
# # |version| and |release|, also used in various other places throughout the
# # built documents.
# # see https://pypi.org/project/setuptools-scm/ for details
# from pkg_resources import get_distribution
# release = get_distribution('{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}').version
# # for example take major/minor
# version = '.'.join(release.split('.')[:2])

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "autodocsumm",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx.ext.mathjax",
    "sphinx.ext.autosummary",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "numpydoc",
    "nbsphinx",
    "m2r2",
    'sphinx_gallery.load_style',
    "IPython.sphinxext.ipython_directive",
    "IPython.sphinxext.ipython_console_highlighting",
    "sphinxcontrib.srclinks",
]


# source_suffix = '.rst'
source_suffix = [".rst", ".md"]

# nbsphinx_custom_formats = {
#     '.md': ['jupytext.reads', {'fmt': 'Rmd'}],
#}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "**.ipynb_checkpoints", "Thumbs.db", ".DS_Store"]

# Optionally, you can make autodocsumm active by default for all autodoc directives by adding in conf.py:
autodoc_default_options = {
    'autosummary': True,
}


# -- nbsphinx specific options ----------------------------------------------
# this allows notebooks to be run even if they produce errors.
nbsphinx_allow_errors = True


# The master toctree document.
master_doc = "index"

# Define nbsphinx_thumbnails for gallery view
nbsphinx_thumbnails = {
   "notebooks/00-example_notebook": "notebooks/images/example_image.png",
}


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# # -- Options for LaTeX output -----------------------------------------------

# latex_elements = {
#     'papersize': 'letterpaper',
#     'pointsize': '10pt',
#     'preamble': '',
#     'figure_align': 'htbp',
# }

# # Grouping the document tree into LaTeX files. List of tuples
# # (source start file, target name, title,
# #  author, documentclass [howto, manual, or own class]).
# latex_documents = [
#     (master_doc, 'th_routing.tex', 'TwinswHeel',
#      'Loic Houpert', 'manual'),
# ]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "osmnx": ("https://osmnx.readthedocs.io/en/stable/", None),
    "networkx": ("https://networkx.org/documentation/stable/", None),
    "scipy": ("https://docs.scipy.org/doc/scipy-1.6.1/reference/", None),
}
